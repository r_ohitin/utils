@echo off

:: Decompile .ert file with gcomp 

 if "%GIT_1S_REPO%"=="" goto :norepovar
 if not exist "%GIT_1S_REPO%\" goto :norepo

 if "%1"=="" goto :nofile

 set FName=%~n1
 set FExt=%~x1
 set FPath=%~dp1
 set FDisk=%~d1

 if "%FExt%"==".ERT" goto :proc_ert
 if "%FExt%"==".ert" goto :proc_ert
 goto :badext

:proc_ert
 set PFolder=
 set ERTFolder=
 set CurPath=%FPath%

:rep
 if "%PFolder%"=="%FDisk%\" goto :notf

 For %%A in ("%CurPath%\.") do (
     Set PFolder=%%~dpA
     Set Folder=%%~nxA
 )
 set ERTFolder=%Folder%\%ERTFolder%
 set CurPath=%PFolder%
 if not exist "%PFolder%\.top" goto :rep

:: Found file ".top". Current dir is name of 1S Config
 For %%A in ("%CurPath%\.") do (
     Set Conf=%%~nxA
 )

 echo Conf is: %Conf%
 echo ERTFolder is: %ERTFolder%
 echo ERT is: %FName%

 gcomp -v -d -F "%FPath%%FName%.ert" -D "%GIT_1S_REPO%\%Conf%\%ERTFolder%SRC"

 goto :EOF

:: Error messages

:notf
 echo File '.top' is not found! 
 echo Create this file in folder that contain 1S Config
 goto :EOF

:nofile
 echo format:
 echo    gd.cmd filename.ert
 goto :EOF

:badext
 echo %FName%%FExt% is not .ERT file
 goto :EOF

:norepovar
 echo Env var GIT_1S_REPO must contain path to 1S repo
 goto :EOF

:norepo
 echo GIT_1S_REPO="%GIT_1S_REPO%" but this path is not found
 goto :EOF
