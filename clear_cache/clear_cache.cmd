@echo off
 
 rem Clear browsers cache
 rem %SYSTEMROOT%\system32\GroupPolicy\User\Scripts\Logoff\clear_cache.cmd

 rem IE 8
 RunDll32.exe InetCpl.cpl,ClearMyTracksByProcess 8

 rem Google Chrome
 del "%USERPROFILE%\Local Settings\Application Data\Google\Chrome\User Data\Default\Cache\*.*" /S /Q /F

 rem Firefox
 for /d %%x in ("%USERPROFILE%\Local Settings\Application Data\Mozilla\Firefox\Profiles\*") do del /q /s /f "%%x\cache2\entries\*"
 
 rem YandexBrowser
 del "%USERPROFILE%\Local Settings\Application Data\Yandex\YandexBrowser\User Data\Default\Cache\*.*" /S /Q /F

 rem Amigo
 del "%USERPROFILE%\Local Settings\Application Data\Amigo\User Data\Default\Cache\*.*" /S /Q /F

 rem Xrom
 del "%USERPROFILE%\Local Settings\Application Data\Xrom\User Data\Default\Cache\*.*" /S /Q /F
